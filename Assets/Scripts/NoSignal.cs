﻿using UnityEngine;

public class NoSignal : MonoBehaviour {
    public bool hasSignal;

    private Material noSignalMaterial;

    void Start() {
        noSignalMaterial = GetComponent<SpriteRenderer>().material;

        noSignalMaterial.SetFloat("_Signal", hasSignal ? 1 : 0);
    }
}