﻿using System.Collections;
using UnityEngine;

public class DissolvingVfx : MonoBehaviour {
    public float dissolveTime;
    public ParticleSystem ashParticles;
    
    private Material _dissolveMaterial;

    void Start() {
        _dissolveMaterial = GetComponent<SpriteRenderer>().material;
        StartCoroutine(Dissolve());
    }

    IEnumerator Dissolve() {
        float fade = 1.0f;
        float elapsed = 0;
        bool particlesStarted = false;
        yield return new WaitForSeconds(1f);
        
        while (fade != 0) {
            elapsed += Time.deltaTime;
            Debug.Log($"Fade: {fade} - elapsed: {elapsed}");
            fade = Mathf.Lerp(1.0f, 0, elapsed / dissolveTime);
            _dissolveMaterial.SetFloat("_Fade", fade);
            if (fade < 0.7f && !particlesStarted) {
                particlesStarted = true;
                ashParticles.Play();
            }
            yield return null;
        }
    }

}